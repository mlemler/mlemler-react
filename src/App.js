import './App.css';
import { AntDesignOutlined, FormatPainterOutlined, HomeOutlined } from '@ant-design/icons';
import { Menu } from "antd";

const items = [
  {
    label: (
        <a href="https://mlemler.de/">Pure HTML and CSS</a>
    ),
    key: 'pure',
    icon: <HomeOutlined />,
  },
  {
    label: (
        <a href="https://mlemler.de/bulma/">Bulma</a>
    ),
    key: 'bulma',
    icon: <FormatPainterOutlined />,
  },
  {
    label: (
        <a href="https://mlemler.de/react/">React and Ant Design</a>
    ),
    key: 'react',
    icon: <AntDesignOutlined />,
  }];

function App() {
  return (
    <Menu theme="dark" mode="horizontal" selectedKeys={['react']} items={items}/>
  );
}

export default App;
