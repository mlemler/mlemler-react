# mlemler.de - React

This is the version of my homepage using React.

## Getting started

### Install the dependencies

```npm
npm ci
```

### Start the local App

To start the App locally run

```npm
npm start
```

## Used Technologies

### Node.js

Of course, we will need [Node.js](https://nodejs.org/en).
I also recommend using the [Node Version Manager](https://github.com/nvm-sh/nvm) to install the needed version.
I will start v20.11.1 which is the current LTS release.

### Create React App

The React Homepage [react.dev](https://react.dev/learn/start-a-new-react-project) proposes to use a React framework like
[Next.js](https://nextjs.org/), [Remix](https://remix.run/) or [Gatsby](https://www.gatsbyjs.com/) to create a React
app.

But since my homepage is not that big and I wanted to use React quite pure I used
[Create React App](https://github.com/facebook/create-react-app) which can be used by the following command.

```npx
npx create-react-app mlemler-react
```